lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'pusherjs/version'

Gem::Specification.new do |s|
  s.name          = "pusherjs"
  s.version       = Pusherjs::VERSION
  s.authors       = ["Nick Chernyshev"]
  s.email         = ["nick.chernyshev@gmail.com"]

  s.summary       = %q{Client JS library for Pusher to include in assets pipeline}
  s.homepage      = "http://github.com/flowerett/pusherjs"
  s.license       = "MIT"

  s.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  s.bindir        = "exe"
  s.executables   = s.files.grep(%r{^exe/}) { |f| File.basename(f) }
  s.require_paths = ["lib"]

  if s.respond_to?(:metadata)
    s.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com' to prevent pushes to rubygems.org, or delete to allow pushes to any server."
  end

  s.add_development_dependency "bundler", "~> 1.9"
  s.add_development_dependency "rake", "~> 10.0"
end
